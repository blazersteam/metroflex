<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'metrofle_gymdb' );

/** MySQL database username */
define( 'DB_USER', 'metrofle_dbusr' );

/** MySQL database password */
define( 'DB_PASSWORD', 'bxWB[!Mdyl%@' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RrfqOJZd,e5${Nkt!u~5`NS*I0fU#kE~bDxpFYBg8G cMn(QQB,K2JCwr6|M43;O' );
define( 'SECURE_AUTH_KEY',  '9b5B7<+#nA0GrI9_@ V1NW&2ciA9|^1x+cNkSpPGC>PC$B/OO3:c|D[.ngWE9_z,' );
define( 'LOGGED_IN_KEY',    'bG5Xl+p`C^g%fx{u<KR5}`aR Yi]0A$dQJO>y[TeSQ9?s{yy`S4a8Fi}9C=NVg}O' );
define( 'NONCE_KEY',        ', 15VEfJZNd(:.L`4WzZy(I{TrV}mOf5.C*/UjD&2DG&[Wemh>ucv$Au/{w.TKea' );
define( 'AUTH_SALT',        'b8><wp0.b4AmjipSwI-dkrePT+c;`H,o/IfJWaB-6}Y0#Ga$=Eg6e)ze{Oo3t&h=' );
define( 'SECURE_AUTH_SALT', '!}?4}=jN[7gIW1Y3E&<B(St7ja|DceU]# U!RFVAEK|2@zfY5q>9Mtk@1^uAR#N#' );
define( 'LOGGED_IN_SALT',   'IkA/A#>j^sJYtMO>sG9D4nYBLpSi.=f*mqfS~GR0N3l7.|o^:#F]k?seXN@zdSuq' );
define( 'NONCE_SALT',       '1iIS</[`5tK^a@To!bzRZh^&1xC2WZrtB_`m{AqMm+bPR|:qFZ[ZX`5y6A3nmc(`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
